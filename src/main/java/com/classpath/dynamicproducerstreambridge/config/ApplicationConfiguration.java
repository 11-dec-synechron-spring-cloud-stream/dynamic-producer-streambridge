package com.classpath.dynamicproducerstreambridge.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.binder.BinderHeaders;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class ApplicationConfiguration {

    private final StreamBridge streamBridge;

    @Bean
    public Supplier<Message<String>> supplyForTargetDestination(){
        return () -> MessageBuilder.withPayload(UUID.randomUUID().toString()).setHeader("reply-to", UUID.randomUUID().toString()).build();
    }

    @Bean
    public Consumer<Message<String>> dynamicProducerUsingStreamBridge(StreamBridge streamBridge) {
        System.out.println("Inside the dynamic producer :: ");
        return (message) -> {
            final Message<String> payload = MessageBuilder
                    .withPayload(message.getPayload())
                    //.setHeader(BinderHeaders.TARGET_DESTINATION, message.getHeaders().get("reply-to"))
                    .build();
            System.out.println(" Inside the dynamicProducerUsingStreamBridge");
            log.info(this.streamBridge.toString());
            streamBridge.send(message.getHeaders().get("reply-to").toString(), payload);
            //streamBridge.send("reply-to-consumer-1", payload);
            //streamBridge.send(message.getHeaders().get("reply-to").toString(), payload);

        };
    }

    @Bean
    public Consumer<String> processMessageConsumer(){
        return (data) -> {
            System.out.println("Received the message");
            System.out.println(data);
        };
    }

}