package com.classpath.dynamicproducerstreambridge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DynamicProducerStreambridgeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DynamicProducerStreambridgeApplication.class, args);
    }

}
